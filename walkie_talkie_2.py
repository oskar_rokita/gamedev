import paho.mqtt.client as mqtt
from colorama import Fore, Style, init
import os
import requests

clear = lambda: os.system('cls')
init(convert=True)
haslo = ''
tries = []
z = 11
game = False

def wisi(haslo,tries):
    global z
    if tries[-1] == haslo:
        return haslo, False
    if len(tries) >z:
        tries = []
        z = 11
        return "GAME OVER! Haslo to : "+haslo, False
    ans = ''
    haslo_t = [x for x in haslo]
    ans_list = [x if x in tries else '_' for x in haslo_t]
    for n in ans_list:
        ans += n
        
    if '_' in ans:
        return ans, True
    else :
        tries = []
        return ans, False
        
# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed
    client.subscribe("twoja")
    client.publish('stara','Miss J joined!')

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    global game, tries, z
    received = str(msg.payload.decode('utf-8'))
    if not game:
        if received == "mow_mi_dobrze":
            resp = requests.get('https://complimentr.com/api')
            msg = resp.json()['compliment']
            client.publish('stara',msg)
            print(msg)
        elif received != "clear":
            print(Fore.CYAN,received, Fore.RESET)
        else:
            clear()
    else:
        if received in tries:
            client.publish('stara',"JUZ BYLO")
        else:
            if received in haslo:
                z+=1
            tries.append(received)
            ans, game = wisi(haslo,tries)
            client.publish('stara',ans+' Left: {}'.format(z-len(tries)))
            if not game:
                tries = []
                client.publish('stara','YOU WIN!')
                print('YOU WIN!')
                z = 11
            print(ans+' Left: {}'.format(z-len(tries)))
    
def on_disconnect(client,userdata,rc):
    if rc != 0:
        print('Unexpected disconnection')

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("broker.hivemq.com", 1883, 60)
client.loop_start()

try:
    while True:
        message = str(input())
        if message == "clear":
            clear()
        elif message.split()[0] == 'game':
            haslo = message.split()[1]
            game = True
            client.publish('stara','WISIELEC')
            continue
        client.publish('stara',message)
except KeyboardInterrupt:
    client.publish('stara',"Miss. J disconnected")
    client.disconnect()
